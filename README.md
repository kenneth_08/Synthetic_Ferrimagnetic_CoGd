# Synthetic Ferrimagnetic CoGd
Master applied physics (nano-track) graduation project @ TU/e
Research group: Physics of Nanostructures (FNA)


## Repository structure
This repository is the root of the project.
The different parts of this project (thesis, computational part, etc.) are checked out in branches.
These different parts/branches are included as submodules in the master branch.

The master branch also has the project description.

Literature, credentials etc. are excluded from git (using .gitignore) for copyright and privacy reasons.

### Development
Thesis, notes and presentations are modified from their 'master' branch.

For the computational submodules there are development branches which are rolled out in the computational/dev folder.
The 'master' branch from computational submodules should only be updated from their dev branches (to maintain
programs in production).

